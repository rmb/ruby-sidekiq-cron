ruby-sidekiq-cron (1.4.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Team upload.
  * Remove constraints unnecessary since buster

  [ Mohammed Bilal ]
  * New upstream version 1.4.0
  * d/control: Bump Standards-Ver to 4.6.1, no changes needed
    - Swtich to ruby:Depends for dependencies
    - add simplecov to Build-Depends
  * Refresh patches
  * remove coverage/ directory

 -- Mohammed Bilal <mdbilal@disroot.org>  Sat, 08 Oct 2022 22:15:49 +0530

ruby-sidekiq-cron (1.2.0-1) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Update team name
  * Add .gitattributes to keep unwanted files out of the source package

  [ Debian Janitor ]
  * Update watch file format version to 4.

  [ Pirate Praveen ]
  * New upstream version 1.2.0
  * Bump Standards-Version to 4.5.1 (no changes needed)
  * Bump debhelper compatibility level to 13

 -- Pirate Praveen <praveen@debian.org>  Tue, 15 Jun 2021 23:41:34 +0530

ruby-sidekiq-cron (1.1.0-4) unstable; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.4.1, no changes needed.

  [ Pirate Praveen ]
  * Bump Standards-Version to 4.5.0 (no changes needed)
  * Drop dependencies on ruby-rufus-scheduler and ruby-redis-namespace

 -- Pirate Praveen <praveen@debian.org>  Fri, 21 Aug 2020 17:57:23 +0530

ruby-sidekiq-cron (1.1.0-3) unstable; urgency=medium

  * Add fixed version for Breaks gitlab

 -- Pirate Praveen <praveen@debian.org>  Fri, 08 Feb 2019 16:03:28 +0530

ruby-sidekiq-cron (1.1.0-2) unstable; urgency=medium

  [ Pirate Praveen ]
  * Reupload to unstable

  [ Utkarsh Gupta ]
  * Add Breaks for gitlab

 -- Pirate Praveen <praveen@debian.org>  Thu, 07 Feb 2019 13:03:34 +0530

ruby-sidekiq-cron (1.1.0-1) experimental; urgency=medium

  * Team upload
  * New upstream version 1.1.0
  * Refresh patches
  * Bump Standards-Version to 4.3.0 (no changes needed)
  * Update d/copyright
  * Update d/control

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 29 Jan 2019 05:56:34 +0530

ruby-sidekiq-cron (0.6.3-5) unstable; urgency=medium

  * Use salsa.debian.org in Vcs-* fields
  * Bump Standards-Version to 4.1.4 (no changes needed)
  * fix gemwatch url
  * Disable more randomly failing tests (Closes: #851317)

 -- Pirate Praveen <praveen@debian.org>  Fri, 13 Jul 2018 09:41:58 +0530

ruby-sidekiq-cron (0.6.3-4) unstable; urgency=medium

  * Disable more tests that give inconsistent results

 -- Pirate Praveen <praveen@debian.org>  Wed, 28 Feb 2018 11:39:09 +0530

ruby-sidekiq-cron (0.6.3-3) unstable; urgency=medium

  * Reupload to unstable
  * Bump standards version to 4.1.3 and debhelper compat to 11

 -- Pirate Praveen <praveen@debian.org>  Fri, 23 Feb 2018 18:48:03 +0530

ruby-sidekiq-cron (0.6.3-2) experimental; urgency=medium

  * Tighten dependencies

 -- Pirate Praveen <praveen@debian.org>  Tue, 19 Sep 2017 13:01:46 +0530

ruby-sidekiq-cron (0.6.3-1) experimental; urgency=medium

  * Team upload
  * New upstream release

 -- Sruthi Chandran <srud@disroot.org>  Mon, 11 Sep 2017 15:35:24 +0530

ruby-sidekiq-cron (0.4.5-1) unstable; urgency=medium

  * Team upload
  * New upstream release

 -- Sruthi Chandran <srud@disroot.org>  Tue, 27 Jun 2017 22:20:49 +0530

ruby-sidekiq-cron (0.4.2-6) unstable; urgency=medium

  * Disable tests that fail when run at specific times (Closes: #844925)

 -- Pirate Praveen <praveen@debian.org>  Sun, 27 Nov 2016 11:51:48 +0530

ruby-sidekiq-cron (0.4.2-5) unstable; urgency=medium

  * Don't exit even if killing redis-server after tests fails.
    This fixes autopkgtest as it allows starting redis-server service
    during dependency installation itself.

 -- Pirate Praveen <praveen@debian.org>  Fri, 02 Sep 2016 00:21:17 +0530

ruby-sidekiq-cron (0.4.2-4) unstable; urgency=medium

  * Team upload.
  * Remove script execution during dh_auto_install and dh_auto_clean
  * Make ruby-tests.rake file to init redis-server and kill it once the
    tests are executed.
  * debian/control: Update standards-version to 3.9.7

 -- Lucas Albuquerque Medeiros de Moura <lucas.moura128@gmail.com>  Thu, 03 Mar 2016 15:17:51 -0300

ruby-sidekiq-cron (0.4.2-3) unstable; urgency=medium

  * Team upload.
  * Add necessary build deps (Closes: #812975)
  * disable-some-tests.patch : Disable a test that may cause FTBFS.
  * Make Vcs-git use https.

 -- Balasankar C <balasankarc@autistici.org>  Sat, 30 Jan 2016 00:40:09 +0530

ruby-sidekiq-cron (0.4.2-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Wed, 27 Jan 2016 21:59:47 +0530

ruby-sidekiq-cron (0.4.2-1) experimental; urgency=medium

  * Initial release (Closes: #811454)

 -- Pirate Praveen <praveen@debian.org>  Tue, 19 Jan 2016 13:11:58 +0530
