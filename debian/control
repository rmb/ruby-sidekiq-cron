Source: ruby-sidekiq-cron
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               rake,
               ruby-fugit,
               redis-server,
               ruby-mocha,
               ruby-rack-test,
               ruby-redis-namespace,
               ruby-shoulda-context,
               ruby-sidekiq,
               ruby-sinatra,
               ruby-simplecov
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-sidekiq-cron.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-sidekiq-cron
Homepage: https://github.com/ondrejbartas/sidekiq-cron
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all

Package: ruby-sidekiq-cron
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${ruby:Depends},
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: gitlab (<< 11.8~)
Description: scheduling add-on for Sidekiq
 Sidekiq Cron helps to add repeated scheduled jobs. It enables to set jobs to
 be run in specified time (using CRON notation)
 .
 It runs a thread alongside Sidekiq workers to schedule jobs at specified times
 (using cron notation `* * * * *` parsed by Rufus-Scheduler
 .
 It checks for new jobs to schedule every 10 seconds and doesn't schedule the
 same job multiple times when more than one Sidekiq worker is running.
 .
 Scheduling jobs are added only when at least one Sidekiq process is running.
