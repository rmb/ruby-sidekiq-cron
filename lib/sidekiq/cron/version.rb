# frozen_string_literal: true

module Sidekiq
  module Cron
    VERSION = "1.4.0"
  end
end
